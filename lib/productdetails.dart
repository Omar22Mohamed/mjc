import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ProductDetails extends StatefulWidget {

  List list;
  int index;

  ProductDetails({this.list, this.index});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {

  void confirm() {

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.list[widget.index]['ProductName']}"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Text(
              widget.list[widget.index]['ProductName'],
              style: TextStyle(fontSize: 20.0),
            ),
            Text(
              widget.list[widget.index]['ProductPrice'],
            ),
            MaterialButton(
              child: Text("EDIT "),
              color: Colors.green,
              onPressed: (){},
            ),
            MaterialButton(
              child: Text("DELETE "),
              color: Colors.red,
              onPressed: ()=>confirm(),
            )
          ],
        ),
      ),
    );
  }
}
