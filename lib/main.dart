import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:mjc/productdetails.dart';

void main() => runApp(MaterialApp(
  title: "MJC Store",
  debugShowCheckedModeBanner: false,
  theme: ThemeData(
    primarySwatch: Colors.red,
  ),
  home: Home(),
));

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  
  Future<List> getData() async{
    final response=await http.get("http://192.168.254.130/mjc/getproducts.php");
    return json.decode(response.body);
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MJC"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){},
        child: Icon(Icons.add),
      ),
      body: FutureBuilder<List>(
        future: getData(),
        builder: (ctx,ss){
          if(ss.hasError)
            {
              print("error");
            }
          if(ss.hasData)
            {
              return Items(list:ss.data);
            }
          else
            {
              return CircularProgressIndicator();
            }
        }
      ),
    );
  }
}

class Items extends StatelessWidget {

  List list;

  Items({this.list});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: list==null ? 0 : list.length,
        itemBuilder: (ctx,i){
          return ListTile(
            leading: Icon(Icons.shopping_basket),
            title: Text(list[i]['ProductName']),
            subtitle: Text(list[i]['ProductPrice']),
            onTap: ()=>Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context)=>ProductDetails(list:list,index:i),
              ),
            ),
          );
        }
    );
  }
}

